# Project Template

This is my personal template for open source projects.

The master branch contains only language-agnostic files needed in every project.
More specific templates are available in other branches:

* C++: https://gitlab.com/s.stegemann/ProjectTemplate/tree/cpp
    * C++/CMake/Doxygen/Catch2: TODO
    * C++/Qt: TODO
* Python: TODO
* TODO